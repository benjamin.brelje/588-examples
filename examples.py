#DOCUMENTATION:
# This is not designed as a first tutorial in Python. It is mainly designed to familiarize you with the numpy and matplotlib packages
# required for Homework 1. If you call this program at the command line "python examples.py" the entry point is below the three function definitions.
# You must have working matplotlib and numpy packages to run this. Installing the correct Anaconda Python for your system should take care of this.


#You need these import statements at the top of your file every time you use a package (like numpy or matplotlib)
#This is an example of a regular import. The math module will be available using the convention "math.sqrt(2)" and so on
import math
#These imports are aliased to nicknames for brevity.
#numpy is commonly shortened to np
import numpy as np
#The pyplot module of Matplotlib is often shortened to plt
import matplotlib.pyplot as plt

#This is a function definition
def examplefunction(x, y=1, maxIter=None):
    #The input x is a positional argument - the first argument - and when this function is called, x MUST be provided
    #y is called a keyword argument. If no value for y is provided, the default value of 1 is used
    #maxIter is another keyword argument which defaults to None


    if maxIter is not None:
        #The following structure iterates through a loop maxIter times, with counter i running from 0 but NOT INCLUDING maxIter
        #The range function creates a list of integers [0,1,2,3,..,maxIter-1]
        z = 0
        for i in range(0,maxIter):
            z = x * y + z
    else:
        #This alternative iteration structure will run until a condition is met 
        z = x
        while np.linalg.norm(z) < 100:
            z = z + y
        #Note that Python functions can have multiple return statements! The first one invoked 'wins' and exits the function
        return z

    z = 2 * z
    #Make sure your logic flow always returns a value from your function. 
    return z

def vectormath(x,y):
    #This function assumes that x is a numpy array of dimension n x 1 (in other words, a vector)
    
    #The dot product is easy. From two vectors it will return a scalar. This is equivalent to z =  tranpose(x) . y
    #Technically, the "dot" function is a method of the "x" object which is a numpy array
    z1 = x.dot(y)
    
    #This is the numpy absolute value function. Be very careful with the built-in Python abs() function
    xabs = np.absolute(x)
    z2 = xabs.dot(y)

    #Python has its own square root function:
    sqrtx1 = math.sqrt(x[0])
    #It's better to use numpy's square root which works on scalars and arrays
    sqrt2 = np.sqrt(2)
    sqrtx = np.sqrt(x)

    #The division operator in Python was changed in Python 3.0. Be very careful if you are using Python 2.7
    py2or3 = 1 / 7
    if py2or3 == 1.0 / 7:
        print 'You must be using Python 3 or have imported its division operator'
    elif py2or3 == 0:
        print 'You are a Python 2.7 user!'
    
    #The power operator can be invoked using the native python operator:
    four = 2 ** 2
    #It also works on numpy arrays:
    xsquared = x ** 2
    #There's also a numpy function which may behave differently:
    xsquared = np.power(x,2)

    #Array indexing works like this for vectors (one dimensional): it accesses the FIRST entry
    x[0] = 1e-9
    #Note how scientific notation works

    return z1, xsquared

def simpleplotfunction(x,y):
    return 2*x - 0.5*y


#You see the following convention a whole lot in Python. It simply means "if this file is being run as a script:"
#In other words, the following code block is the entry point if you run "python myfile.py" at the command line
if __name__ == "__main__":
    this_vector = np.array([0,1,0,4])
    second_vector = np.array([2,0,5,0])

    #The following illustrates a few ways to call functions
    print 'With all default options:'
    z = examplefunction(1)
    print z

    #You can also feed in an array/vector without changing any of the underlying source:
    #Python does not have strong data types! My function doesn't care whether you feed it an int, a double, or a numpy array
    #Note that not all functions will support numpy arrays, but all numpy functions will support ints and doubles
    print 'With a vector as input:'
    z = examplefunction(this_vector)
    print z

    #You can supply an option other than the defaults:
    print 'With optional second input:'
    z = examplefunction(this_vector,y=second_vector)
    print z

    #Revisit the first one with a different number of iterations:
    print 'With specified number of iterations:'
    z = examplefunction(1,maxIter=25)
    print z
    
    #Let's run the vectormath program. The output should be a dot product of the two vectors as well as an element-wise squaring of the first vector
    dottedvectors, vectorsquared = vectormath(this_vector,second_vector)
    print 'x . y = '
    print dottedvectors
    print 'x .^2 = '
    print vectorsquared

    #As expected, these vectors dot to zero. Let's dot a vector with itself and apply an identity:
    dottedvectors, vectorsquared = vectormath(this_vector,this_vector)
    vec_magnitude = np.linalg.norm(this_vector,ord=2)
    unity = dottedvectors / vec_magnitude ** 2
    print 'This should be one:'
    print unity


    #Now for some plots. Here's a little recipe for an x-y scatter plot
	
	#Generate a little data:
    xvec = np.arange(0,15,step=1)
    plotdata = np.random.normal(size=15)
	
	#Now actually make the figure
    plt.figure()
    plt.plot(xvec,plotdata)
    plt.title('My plot title')
    plt.xlabel('Sequential x axis')
    plt.ylabel('Random y data')

    print 'The program will pause until you shut the plot window'
    plt.show()

    #This recipe creates a contour plot on the interval x=0 to 15, y=0 to 20
    #and plots simpleplotfunction(x,y) on the z axis with labeled contour lines
	
	#The linspace function is similar to the MATLAB function of the same name
    xvec = np.linspace(0,15,16)
    yvec = np.linspace(0,20,21)
    Xgrid, Ygrid = np.meshgrid(xvec,yvec)
    Zgrid = np.zeros((len(yvec),len(xvec)))
	#Every contour plot needs X, Y, and Z grids in two dimensions
	
	# Now fill in the data. 
    # You can sometimes do this vectorially but I'll fill in element
    # by element for now
    for i, xval in enumerate(xvec):
        for j, yval in enumerate(yvec):
            xval = xvec[i]
            yval = yvec[j]
            #note that these indices are reversed. Just the way it is
            Zgrid[j][i] = simpleplotfunction(xval,yval)
    
    plt.figure()
    CS = plt.contour(Xgrid,Ygrid,Zgrid)
    #This creates the contour plot line labels
    plt.clabel(CS, inline=1, fontsize=10)
    plt.title('Contour plot')
    #these three entries plot dots in the XY plane with different markers and colors
    plt.plot(5,5,'r.')
    plt.plot(10,5,'b.')
    plt.plot(10,10,'k*')
    #annotate your plot with text at a given XY location
    plt.text(10,10-.75,'Initial point')
    plt.xlabel('X axis')
    plt.ylabel('Y axis')
    plt.show()



